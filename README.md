# README #

это простая оболочка для mencoder для сборки таймлапсов. 

Главный файл - wxtimelapse.py. 
timelapse.py содержит класс проекта, класс изображения в проекте.

Требуются библиотеки:
wxPython (wxGtk возможно), skimage (он потянет за собой SciPy, NumPy).

Пока что требует mencoder для сборки и mplayer для просмотра.
[Пример таймлапса](https://yadi.sk/d/CwmE6EYUis9NA)