#! -*- coding: utf8 -*-
#!/usr/bin/env python
u"""
wxtimelapse.py - главный модуль программы для сборки таймлапсов
"""
import wx, os, sys
import Image
from os.path import splitext, join

from wx import StaticText, ComboBox, TextCtrl, Button, CheckBox
from wx.lib.ticker import Ticker
#from wx.lib.pubsub import Publisher
from threading import Thread

from timelapse import Project, writeProject, loadProject, convert_raw
from shot import ShotFrame



confpanel = None
filelist = None
progfile =  os.path.abspath(sys.argv[0])
progdir = os.path.dirname(progfile)

img_types_dict = {'jpg':wx.BITMAP_TYPE_JPEG, 'jpeg':wx.BITMAP_TYPE_JPEG, 
	'png': wx.BITMAP_TYPE_PNG} 


project = None




class Frame(wx.Frame):
	u"""Основной фрейм"""
	def __init__(self, master, title):
		wx.Frame.__init__(self, master)
		grid = wx.GridBagSizer(hgap=5, vgap=5)
		
		self.SetTitle(u"wxTimelapse - простая сборка таймлапса")
		self.confpanel = ConfFrame(self)
		self.filelist = FileListPanel(self)
		self.buttons = ButtonPanel(self)
		self.timeline = TimeLine(self)
					
		grid.AddGrowableRow(2)
		grid.AddGrowableRow(1)
		grid.AddGrowableCol(1)
		grid.AddGrowableCol(0)
		grid.Add(self.confpanel, (1,1), flag=wx.ALL, border=5)
		grid.Add(self.buttons, (0,0),  flag=wx.ALL, border=5)
		grid.Add(self.filelist, (1,0),  flag=wx.EXPAND|wx.ALL)	
		grid.Add(self.timeline, (2,0), (3,2), flag=wx.EXPAND|wx.ALL)
			
		menuBar = wx.MenuBar()
		menu1 = wx.Menu()
		menu1.Append(101, u"&Открыть проект")
		menu1.Append(103, u"Сохранить проект")
		menu1.AppendSeparator()
		menu1.Append(104, u"Выход")
		menuBar.Append(menu1, u"Проект")
		self.Bind(wx.EVT_MENU, self.confpanel.LoadProject, id=101)
		self.Bind(wx.EVT_MENU, self.filelist.SaveProject, id=103)
		self.Bind(wx.EVT_MENU, self.buttons.quit, id=104)
		menu2 = wx.Menu()
		menu2.Append(201, u"Добавить файлы из папки")
		menu2.Append(202, u"Очистить список")		
		menuBar.Append(menu2, u"Список")
		self.Bind(wx.EVT_MENU, self.filelist.LoadFiles, id=201)
		self.Bind(wx.EVT_MENU, self.filelist.clearListbox, id=202)
		
		self.SetMenuBar(menuBar)	
		self.SetSizerAndFit(grid)
	
	#/Frame

class FileListPanel(wx.Panel):
	u""" Панель списка файлов """
	def __init__(self, master):
		wx.Panel.__init__(self, master)
		self.parent = master
		self.ext = 'jpg'
		grid = wx.GridBagSizer(hgap=5, vgap=5)
		title = wx.StaticText(self, -1, u'Список изображений')
		self.listbox = wx.ListBox(self, 60, (100, 50), (90, 120), [], wx.LB_SINGLE)
		self.img_list = []
		self.dir_files_list = []
		grid.Add(title, (0,0))
		grid.AddGrowableRow(1)
		grid.AddGrowableCol(0)
		grid.Add(self.listbox, (1,0), flag=wx.EXPAND|wx.ALL, border=5)
		self.progress = wx.Gauge(self, range=100)
		
		jpg = wx.Image('0.JPG', wx.BITMAP_TYPE_JPEG).ConvertToBitmap()
		self.image = wx.StaticBitmap(self, -1, jpg, (10, 0), (jpg.GetWidth(), jpg.GetHeight()))
		grid.Add(self.image, (2,0), border=10, flag=wx.EXPAND|wx.ALL)
	
		del_image = Button(self, -1, u'Удалить из списка')
		edit_image = Button(self, -1, u'Редактировать')
		del_image.Bind(wx.EVT_BUTTON, self.deleteImage)
		
		b_sizer = wx.BoxSizer(wx.HORIZONTAL)
		b_sizer.Add(del_image, 0)
		b_sizer.Add(edit_image, 0 )
		grid.Add(b_sizer, (3,0))
		grid.Add(self.progress, (4,0), border=5, flag=wx.ALL|wx.EXPAND)
		
		self.listbox.Bind(wx.EVT_LISTBOX, self.onSelectFile)
		self.SetSizerAndFit(grid)
		
		#Publisher().subscribe(self.setProgress, "update")
	
	#FileListPanel
	def deleteImage(self, evt):
		i = self.listbox.GetSelection()
		self.listbox.Delete(i) 
		print 'Delete', i

	def clearListbox(self, evt):
		self.listbox.Clear()

	def onSelectFile(self, evt):
		filename = evt.GetString()
		print filename
		img_type = img_types_dict[self.ext]
		if filename:
			jpg = wx.Image(filename, img_type).Rescale(500,333, wx.IMAGE_QUALITY_NORMAL).ConvertToBitmap()
			self.image.SetBitmap(jpg)
	
	def selectFileByNum(self, num)	:
		img_type = img_types_dict[self.ext]
		filename = self.img_list[num]
		if filename:
			jpg = wx.Image(filename, img_type).Rescale(500,333, wx.IMAGE_QUALITY_NORMAL).ConvertToBitmap()
			self.image.SetBitmap(jpg)
	
		
	def getFileList(self):
		return [self.listbox.GetString(i).encode("utf8")+"\n" for i in range(self.listbox.GetCount())]

	#FileListPanel
	def setFileList(self, lst):
		self.listbox.Clear()
		self.img_list = lst
		for item in lst:
			if len(item) >2 :
				self.listbox.Append(item)


	def LoadFiles(self, ev): 
		dlg = wx.DirDialog(self, "Choose a directory:",
                          style=wx.DD_DEFAULT_STYLE
                           | wx.DD_DIR_MUST_EXIST
                           | wx.DD_CHANGE_DIR
                           )

		if dlg.ShowModal() == wx.ID_OK:
			print dlg.GetPath()
		else:
			return

		self.directory = directory = dlg.GetPath()
		
		file_list = []
		for top, dirs, files in os.walk(directory):
			for nm in files:       
			   file_list.append(os.path.join(top, nm))

		file_list.sort()
		self.dir_files_list = file_list
		
		if self.ext == 'CR2':
			#file_list = convert_raw(file_list)
			pass
			
			
		for item in file_list:
			if splitext(item)[1].lower()=="."+self.ext:
				self.listbox.Append(item)
				self.img_list.append(item)

		
		self.parent.confpanel.directory = directory
		
		self.progress.SetRange(len(self.img_list))
		if self.parent.buttons.loadCheck.GetValue():
		
			worker = Thread(target=project.loadFromList, args=(self.img_list, self.setProgress))     
			worker.setDaemon(True)    
			worker.start()


		#project.loadFromList(self.img_list, self.setProgress) # Удалось загрузить фотки в отдельном потоке
		
		print self.parent.confpanel.directory
		self.parent.timeline.pos_slider.SetMax(len( self.img_list ))
		dlg.Destroy()

	#FileListPanel
	def SaveProject(self, ev):
		writeProject(project, project.name+'.tlp' )


	def setProgress(self, i):
		self.progress.SetValue(i)
		print i

	def setTypeFilter(self, ext):
		self.ext = ext
		self.listbox.Clear()
		for item in self.dir_files_list:
			if splitext(item)[1].lower()=="."+self.ext: 
				self.listbox.Append(item)
				self.img_list.append(item)
		
		project.filelist = self.img_list
		#/FileListPanel	
		
		
	
class ConfFrame(wx.Panel):
	u"""Панель настроек проекта/рендера"""
	def __init__(self, master):
		wx.Panel.__init__(self, master)
		grid = wx.GridBagSizer(hgap=5, vgap=5)
		self.parent = master

		box = wx.StaticBox(self, -1, u"Параметры видео:")
		bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)
		
		
		w0 = StaticText(self, label=u'Название видео')
		grid.Add(w0, (0, 0))
		self.name = TextCtrl(self)
		grid.Add(self.name, (0, 1))
		
		w4 = StaticText(self, label=u'Тип файлов')
		self.ext_combo = ComboBox(self, choices = [u"jpg", u"png", u"CR2"],style=0)
		self.ext_combo.SetStringSelection(u"jpg")
		self.ext_combo.Bind(wx.EVT_COMBOBOX, self.changeImgType)
		grid.Add(w4, (1,0))
		grid.Add(self.ext_combo, (1,1))
		
		w1 = StaticText(self, label=u'Ширина видео')
		#grid.Add(w0, (1,0))
		grid.Add(w1, (2,0))
        #ConfFrame 
		self.width_box = ComboBox(self, -1, value=u"1920",  choices = [u"1920",u"1080",u"720"], style=0)
		grid.Add(self.width_box, (2,1))
		self.width_box.SetSelection(0)

		w2 = StaticText(self, label=u'Частота кадров')
		grid.Add(w2, (3,0))
		self.fps_combo = ComboBox(self,-1, value=u"30", choices = [u"15",u"20",u"24", u"30", u"48", u"60", u"120"],style=0)
		grid.Add(self.fps_combo, (3,1))
		self.fps_combo.SetSelection(0)
		#ConfFrame
		w3 = StaticText(self, label=u'Кодек')
		grid.Add(w3, (4, 0))
		self.codec_combo = ComboBox(self, choices = [u"msmpeg4",u"mpeg4",u"h261", u"flv", u"wmv2", u"mjpeg", u"ljpeg"],style=0)
		grid.Add(self.codec_combo, (4, 1))
		self.codec_combo.SetSelection(0)

		w3 = StaticText(self, label=u'Битрейт')
		grid.Add(w3, (5, 0))
		self.vbitrate = TextCtrl(self)
		grid.Add(self.vbitrate, (5, 1))
		self.vbitrate.WriteText('16000')#.insert('0', '16000')

		w4 = StaticText(self, label=u'ovc')
		grid.Add(w4, (6, 0))
		self.ovc = ComboBox(self, choices = [u"lavc",u"copy", u"lavcopts", u"x264"],style=0)
		grid.Add(self.ovc, (6, 1))
		self.ovc.SetSelection(0)

		w5 = StaticText(self, label=u'Звук на фон')
		grid.Add(w5, (7, 0))
		self.sound = TextCtrl(self)
		grid.Add(self.sound, (7, 1))
		


		
		#ConfFrame
		execute_button = Button(self, -1,u'Собрать!', )
		execute_button.Bind(wx.EVT_BUTTON, self.execute)
		grid.Add(execute_button, (8,1))
		
		play_button = Button(self, -1,u'Посмотреть!', )
		play_button.Bind(wx.EVT_BUTTON, self.play)
		grid.Add(play_button, (9,1))
		
		
		
		self.ext = 'jpg'
		self.directory = ''

		global project
		project = Project()
		bsizer.Add(grid, 1, wx.EXPAND|wx.ALL, border=5)
		self.SetSizerAndFit(bsizer)
		self.name.Bind(wx.EVT_TEXT, self.syncWithProject)
	
	#ConfFrame


	def syncWithProject(self, evt):
		project.name = self.name.GetValue()
		project.width = self.width_box.GetStringSelection()
		project.codec = self.codec_combo.GetStringSelection()
		project.filelist = self.parent.filelist.getFileList()
		project.directory = self.directory
		project.fps = self.fps_combo.GetStringSelection()
		project.ovc = self.ovc.GetStringSelection()
		project.filetype = self.ext
		project.ext = self.ext
		project.sound = self.sound.GetValue()
		
	def changeImgType(self, evt):
		project.filetype = self.ext = self.ext_combo.GetStringSelection()
		self.parent.filelist.setTypeFilter(self.ext)
		self.parent.filelist.ext = self.ext
		
	#ConfFrame
	def execute(self, evt):
		project.directory = self.directory
		project.filetype = self.ext
		project.width = self.width_box.GetStringSelection()
		project.codec = self.codec_combo.GetStringSelection()
		project.name = self.name.GetValue()
		project.filelist = self.parent.filelist.getFileList()
		project.fps = self.fps_combo.GetStringSelection()
		print  project.name, project.directory
		project.ovc = self.ovc.GetStringSelection()
		project.sound = self.sound.GetValue()
		project.render_filelist()
        
	#ConfFrame
	def LoadProject(self, ev):
		dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard="*.tlp",
            style=wx.OPEN 
            )

		if dlg.ShowModal() == wx.ID_OK:
			paths = dlg.GetPaths()
		
		project = loadProject(dlg.GetPaths()[0])
		print project.width, project.name
		self.directory = project.directory
		self.ext = project.filetype
		self.width_box.SetStringSelection(str(project.width))
		self.codec_combo.SetStringSelection(project.codec)
		self.name.SetValue(project.name)
		self.parent.filelist.ext = self.ext
		self.parent.filelist.setFileList( project.filelist )
		
		self.parent.timeline.pos_slider.SetMax(len(project.filelist ))
		
	def play(self, ev):
		c = u'mplayer \'%s\'' % os.path.join(project.directory, project.name + '.avi')
		print 'run ' +  c
		os.system(c)
		
		
	#ConfFrame	
	def SaveFile(self, ev):
		fn = tkFileDialog.SaveAs(root, filetypes = [('*.timelapse project', '.tpj')]).show()
		if fn == '':
			return

		project.save(fn)




class TimeLine(wx.Panel):
	u"""Будущая линейка времени"""
	def __init__(self, master):
		wx.Panel.__init__(self, master)
		self.parent = master
		sizer = wx.BoxSizer(wx.VERTICAL)
		self.pos_slider = wx.Slider(self, value=self.parent.filelist.listbox.GetSelection(), minValue=1, maxValue=100,
                        size=(150,-1),
                        style=wx.SL_HORIZONTAL|wx.SL_AUTOTICKS|wx.SL_LABELS)

		#if os.path.exists(progfile+'_tmp.png'):
		#	jpg = wx.Image(progfile+'_tmp.png', wx.BITMAP_TYPE_PNG).Scale(master.GetSize()[1]-10, 150).ConvertToBitmap()
		#else:
		#	jpg = wx.Image(join(progdir,'graph-blank.JPG'), wx.BITMAP_TYPE_JPEG).Scale(master.GetSize()[1]-10, 150).ConvertToBitmap()
		
		#self.image = wx.StaticBitmap(self, -1, jpg, (0, 0), (jpg.GetWidth(), 30))		
		sizer.Add(self.pos_slider, 1, wx.EXPAND|wx.ALL)
		
		
		self.pos_slider.Bind(wx.EVT_SCROLL, self.onChange)
		
		self.SetSizerAndFit(sizer)
		#sizer.Add(self.image , 1, wx.EXPAND|wx.ALL)
		self.SetSizerAndFit(sizer)
		
	
	def onChange(self, evt):
		self.parent.filelist.selectFileByNum(self.pos_slider.GetValue())
		self.parent.filelist.listbox.SetSelection(self.pos_slider.GetValue())


class ButtonPanel(wx.Panel):
	u"""Панель с кнопками"""
	def __init__(self, master):
		wx.Panel.__init__(self, master)
		self.parent = master
		loadBtn = Button(self, -1, u'Добавить файлы')
		saveBtn = Button(self,  -1, u'Сохранить проект')
		shotBtn = Button(self,  -1, u'Снять таймлапс')
		quitBtn = Button(self,  -1,   u'Выход')
		analize = Button(self,  -1,   u'Анализировать фотки')
		adjust = Button(self,  -1,   u'Сгладить скачки яркости')
		
		loadCheck = CheckBox(self, -1, u'Загрузка и анализ' )
		loadCheck.SetValue(False)
		grid = wx.GridBagSizer(hgap=5, vgap=5)
		self.loadCheck = loadCheck
		
		loadBtn.Bind(wx.EVT_BUTTON, master.filelist.LoadFiles)
		saveBtn.Bind(wx.EVT_BUTTON, master.filelist.SaveProject)
		shotBtn.Bind(wx.EVT_BUTTON, self.showShotFrame)
		analize.Bind(wx.EVT_BUTTON, self.analizePhotos)
		quitBtn.Bind(wx.EVT_BUTTON, self.quit)
		adjust.Bind(wx.EVT_BUTTON,  self.adjustHist)

		grid.Add(loadBtn, (0,0))

		grid.Add(saveBtn, (0,1))
		grid.Add(analize, (0,3))
		grid.Add(shotBtn, (0,2))
		grid.Add(adjust, (0,4))
		grid.Add(quitBtn, (0,5))
		grid.Add(self.loadCheck, (0,6))

		self.SetSizerAndFit(grid)

	def showShotFrame(self, evt):
		shotFrame = ShotFrame(self, -1)
		shotFrame.Show(True)

	def quit(self, ev):
		self.parent.Close(True)

	def analizePhotos(self, evt):
		u"""Запуск сбора данных о гистограммах фотографий, показ графика"""
		h0, h1, fn, dh = project.getHistogram2Graph()
		png = wx.Image(fn, wx.BITMAP_TYPE_PNG).Rescale(500,333, wx.IMAGE_QUALITY_NORMAL).ConvertToBitmap()
		self.parent.filelist.image.SetBitmap(png)
		
	def adjustHist(self, evt):
		print 'try to adjust photos'
		project.adjustHist()
		

app = wx.App(False)  # Create a new app, don't redirect stdout/stderr to a window.
frame = Frame(None, wx.ID_ANY) # A Frame is a top-level window.
frame.Show(True)     # Show the frame.
app.MainLoop()
