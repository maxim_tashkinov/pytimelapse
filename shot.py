#! -*- coding: utf8 -*-
#!/usr/bin/env python
u"""Модуль окна захвата фотографий с RaspberryPI или поддерживаемого gphoto2 фотика"""
import wx, os, sys
from wx import StaticText, ComboBox, TextCtrl, Button
from os.path import splitext
from timelapse import ShotProject, writeProject, loadProject
#from os import Popen

from subprocess import Popen, PIPE

progfile =  os.path.abspath(sys.argv[0])

def testCameras():
	s = u"gphoto2 -l"
	Popen(s, shell=True, stdin=PIPE, stdout=PIPE).stdout.read().split()


class ShotFrame(wx.Frame):
	u"""Окно захвата таймлапса"""
	def __init__(self, master, title):
		wx.Frame.__init__(self, master)
		grid = wx.GridBagSizer(hgap=5, vgap=5)
		
		self.SetTitle(u"Захват изображений")
		self.shotConfFrame = ShotConfFrame(self)

		grid.Add(self.shotConfFrame, (1,1))

		menuBar = wx.MenuBar()
		menu1 = wx.Menu()
		menu1.Append(101, u"&Открыть параметры захвата")
		menu1.Append(103, u"Сохранить параметры")
		menu1.AppendSeparator()
		menu1.Append(104, u"Выход")
		menuBar.Append(menu1, u"Захват")
		#self.Bind(wx.EVT_MENU, self.shotConfFrame.LoadProject, id=101)
		#self.Bind(wx.EVT_MENU, self.filelist.SaveProject, id=103)
		
		self.SetMenuBar(menuBar)	
		self.SetSizerAndFit(grid)
	
	#/Frame



class ShotConfFrame(wx.Panel):
	u"""Панель настроек таймлапса"""
	def __init__(self, master):
		wx.Panel.__init__(self, master)
		box = wx.StaticBox(self, -1, u"Параметры фото:")
		bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)
		
		grid = wx.GridBagSizer(hgap=5, vgap=5)
		self.parent = master
		
	
			
		w1 = StaticText(self, label=u'Ширина фото')

		grid.Add(w1, (2,0))
        #ShotConfFrame 
		self.width_box = ComboBox(self, -1, value=u"1920",  choices = [u"5202",u"1920",u"1080",u"720"], style=0) #добавить размеров
		grid.Add(self.width_box, (2,1))
		self.width_box.SetSelection(0)

		w2 = StaticText(self, label=u'Интервал между спусками')
		grid.Add(w2, (3,0))
		self.interval = ComboBox(self,-1, value=u"30", choices = [u"1",u"2",u"3",u"5", u"8", u"10", u"12", u"15",u"20",u"25", u"30", u"48", u"60", u"120"],style=0)
		grid.Add(self.interval, (3,1))
		self.interval.SetSelection(0)
		#ShotConfFrame
		
		w3 = StaticText(self, label=u'Выдержка')
		grid.Add(w3, (4,0))
		self.time = ComboBox(self,-1, value=u"30", choices = [u"1",u"2",u"3",u"5", u"8", u"10", u"12", u"15",u"20",u"25", u"30", u"48", u"60", u"90", u"120", u"300"],style=0)
		grid.Add(self.time, (4,1))
		self.time.SetSelection(0)
		
		
		w4 = StaticText(self, label=u'Формат')
		grid.Add(w4, (5, 0))
		self.format_combo = ComboBox(self, choices = [ u"jpeg", u"raw"],style=0)
		grid.Add(self.format_combo, (5, 1))
		self.format_combo.SetSelection(0)

		w5 = StaticText(self, label=u'Основа имени файлов на выходе')
		grid.Add(w5, (6, 0))
		self.name = TextCtrl(self)
		grid.Add(self.name, (6, 1))

		wt = StaticText(self, label=u'Камера:')
		self.camera = ComboBox(self,-1, value=u"Canon with Gphoto2", choices = [u"Rpi Camera", u"Canon with Gphoto2"],style=0)
		grid.Add(wt, (7,0))
		grid.Add(self.camera, (7,1))
		
		execute_button = Button(self, -1,u'Камера, мотор!', )
		execute_button.Bind(wx.EVT_BUTTON, self.IntervalTimer)
		grid.Add(execute_button, (8,1))
		self.ext = 'jpg'
		self.directory = ''

		self.shot_project = ShotProject()
		
		self.Bind(wx.EVT_TIMER, self.IntervalTimer)
		
		bsizer.Add(grid, 1)
		
		self.shot_no = 0
		self.SetSizerAndFit(bsizer)
		#self.name.Bind(wx.EVT_TEXT, self.syncWithProject)
	
	#ShotConfFrame
	def IntervalTimer(self, evt):
		self.t1 = wx.Timer(self)
		self.t1.Start(1000 * int(self.interval.GetValue()))
		print "EVT_TIMER timer started\n"
		self.getFrame(evt)
		
		
	def makeFolderForTimelapse(name):
		if not os.path.exists(name):
			pass
			
		
	def getFrame(self, evt):
		if self.camera.GetValue() == u"Rpi Camera":
			print u"Пробуем получить фото с R PI"
			s = "raspistill -o %s_%06d.jpg" % (self.name.GetValue(), self.shot_no)
			print s
			Popen(s, shell=True, stdin=PIPE, stdout=PIPE).stdout.read().split()
		
		elif self.camera.GetValue() == u"Canon with Gphoto2":
			print u"Пробуем получить фото с Canon"
			s = u"gphoto2 --capture-image -F 1 -I 1 --filename ./%s_%06d.jpg" % (self.name.GetValue(), self.shot_no)
			Popen(s, shell=True, stdin=PIPE, stdout=PIPE).stdout.read().split()
			
		else:
			print u"Камера не задана!"
		
		self.shot_no += 1


if __name__ == "__main__":
	testCameras()
	app = wx.App(False)  # Запустимся автономно от сборки, раз уж хочется
	frame = ShotFrame(None, wx.ID_ANY) 
	frame.Show(True)   
	app.MainLoop()
