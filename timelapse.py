#! -*- coding: utf8 -*-
#!/usr/bin/python
u"""
Модуль с классами проекта
"""
import os, sys, shutil
from ttk import Combobox
import ttk
from threading import Thread

import PIL.Image as Image

try:
	from skimage import io
	from skimage.filter import denoise_tv_chambolle, denoise_bilateral, threshold_otsu
	from skimage.exposure import equalize_hist
	from skimage import exposure
	from skimage import transform
	print 'skimage  OK'
except:
	print 'for debian-based: \n sudo apt-get install python-skimage'
	
import matplotlib.pyplot as plt

# импортирование модулей python
from Tkinter import Entry, Label, Button, Frame, Tk
import tkFileDialog
import pickle

from subprocess import Popen, PIPE
import numpy as np
from os.path import join, abspath


progfile =  abspath(sys.argv[0])
progdir = abspath(os.path.dirname(progfile))

print progdir


if __name__=="__main__":
	print u'Это модуль проекта'
 


#mencoder "mf://*.png" -vf scale=1920:-11 -oac copy -ovc lavc -lavcopts vcodec=msmpeg4:vbitrate=16000 -ffourcc MP43 -o "sky-1.avi"




def bright(P):
	r,g,b = P
	return 0.299*r + 0.587*g + 0.114*b

def writeProject(project, filename):
		u"""Запись проекта в файл"""
		f = open(filename, 'w')
		pickle.dump(project, f)
		f.close()


def loadProject(filename):
	u"""Загрузка проекта из файла"""
	f = open(filename, 'r')
	rpr = pickle.load(f)
	f.close()
	return rpr


def convert_raw(raw_list, name='', fast=False):
	u""" Принимает список рав-ов и имя проекта, возвращает список файлов после конвертирования"""
	if not raw_list: return
	if name == '':
		new_folder = os.path.join(os.path.abspath(sys.argv[0]), 'tmp_for_convert')
	else:
		new_folder =  os.path.join(os.path.abspath(sys.argv[0]), name)
		
	if not os.path.exists(new_folder):
		os.makedirs(new_folder)
	else:
		shutils.rmtree(new_folder)
		os.makedirs(new_folder)
	
	for raw in raw_list:
		s = u'dcraw -v -w -H 1 -o 0 -q 1 -4 -T -b 2  ' + raw
		Popen(s, shell=True, stdin=PIPE, stdout=PIPE).stdout.read().split()
		
		
def resizeImage(fn, width):
	im1 = Image.open(fn)
	w, h = im1.size	
	r = width*1.0/w
	height = int(h*r)
	
	im1 = im1.resize((width, height))
	
	new_folder = os.path.join(os.path.dirname(fn), 'resized')
	
	if not os.path.exists(new_folder):
		os.makedirs(new_folder)
	
	nf = os.path.join(new_folder, os.path.basename(fn))
	im1.save(nf)
	return nf
		

class Project():
	u""" 
	Класс проекта, должен будет уметь сохранять и загружать все параметры, запускать рендер, анализировать фотки
	"""
	def __init__(self, name=u'Новый', directory='', filetype='JPG', filelist='', fps=25, vbitrate=16000, codec='msmpeg4', width=1920, sound_file='', ovc='lavc'):
		self.name = name
		self.directory = directory
		self.filetype = filetype
		self.filelist = filelist
		self.fps = fps
		self.vbitrate = vbitrate
		self.codec = codec
		self.width = width
		self.sound = sound_file # надо узнать как добавить звук
		self.ovc = ovc
		self.hist0 = []
		self.histogram0 = []
		self.brightnesses = []
		
		
	def __unicode__(self):
		return u'Project (%s) %s ' % (self.directory, self.name )	
		
		
	def __str__(self):
		return u'Project (%s) %s ' % (self.directory, self.name )
		
	def render(self, method='mencoder'):
		u"""ф-я устаревшая, оставлена как пример"""
		s = u'cd "%s" && mencoder "mf://*.%s" -vf scale=%s:-11 -oac copy -ovc lavc -lavcopts vcodec=%s:vbitrate=%s -ffourcc MP43 -o "%s.AVI"' %(\
		   self.directory, self.filetype,  self.width, self.codec, self.vbitrate, self.name)
		print 'execute ',  s
		Popen(s, shell=True, stdin=PIPE, stdout=PIPE).stdout.read().split()
		

	def render_filelist(self, method='mencoder'):
		u"""функция рендера по списку файлов"""
		f = open(self.name+'_tmp.list', 'w+')
		f.write(''.join(self.filelist))
		f.close()
		
		s0 = u'cd "%s" && mencoder "mf://@%s_tmp.list" -mf fps=%s -vf scale=%s:-11 ' % (self.directory, self.name,  self.fps, self.width)
		if self.sound: 		s1 = u'-oac copy  -audiofile "%s" ' % (self.sound, )
		else: s1 = u' -oac copy '
		s2 = u' -ovc %s -lavcopts vcodec=%s:vbitrate=%s -ffourcc MP43 -o "%s.AVI" ' % (self.ovc, self.codec, self.vbitrate, self.name )
		
		s = s0 + s1 + s2
		
		print u'execute #\n' + s + '\n'
		Popen(s, shell=True, stdin=PIPE, stdout=PIPE).stdout.read().split()
		
	def analize(self, lst=None, callback=None):
		u""" Экспериментальная ф-я, вызывалась по кнопке Анализировать """
		if not lst: lst = self.filelist
		print '\nanalize'
		prev_img = io.imread(lst[0])
		result = {}
		print type(prev_img)
		thresholds = []
		histogramms = []
		
		h1s, h2s = [], []
		i = 0
		for filename in lst:
			img = io.imread(filename)
			
			hv, hc = exposure.histogram(img, nbins=2)
			h1s.append(hv[0])
			h2s.append(hv[1])
			
			histogramms.append((hv, hc))
			
			if not callback: 
				print '.', 
			
			else:
				callback(i)
				i+=1

		print h1s
		
		plt.plot(h1s)
		#plt.plot(h2s)
		plt.ylabel('values')
		plt.show()	
						
		return result
			
	def loadFromList(self, filelist, callback=None):
		u"""Загрузка в проект фотографий из списка"""
		self.filelist = filelist
		self.tlImages = []
		
		i=0
		for fn in filelist:
			self.loadResizedFile(fn)
			i+=1
			if callback:
				callback(i)
			
			
	def loadFile(self, filename):
		u"""Загрузка одного файла в проект"""
		print 'load file ', filename
		tli = TLImage(filename)
		self.tlImages.append(tli)
		self.brightnesses.append(tli.histogram[0])
		print 'load file end'
		
		
	def loadResizedFile(self, filename):
		u"""Загрузка одного файла в проект"""
		print 'load file ', filename
		size = self.width
		tli = TLImage(resizeImage(filename, size))
		self.tlImages.append(tli)
		self.brightnesses.append(tli.histogram[0])
		print 'load file end'	
	
		
			
	def getHistogram2Graph(self):
		u""" Получение гистограммы для дальнейшего анализа """
		histogram1 = []
		histogram0 = []
		
		for tli in self.tlImages:
			histogram0.append(tli.histogram[0])#[0][0])
			histogram1.append(tli.histogram[1])#[0][1])
		
		#plt.fig.set_size_inches(5,1)	
		plt.plot(histogram0)
		plt.plot(histogram1)
		plt.ylabel('values')
		plt_file = os.path.dirname(progfile)+'_plt.png'
		plt.savefig(plt_file)
				
		delta_h = []
		for i in xrange(1, len(self.tlImages)-1):
			h0 = histogram0[i - 1]
			h1 = histogram0[i]
			h2 = histogram0[i + 1]
			delta_h.append(h1-h0)
			print 'delta \t2-1, \t1-0, \t % '
			print '\t', (h2-h1), '\t', (h1-h0), '\t', 100.0* (h2 - h0) / h1
		
		self.hist0 = histogram0
		self.hist1 = histogram1 
		self.delta_h = delta_h
		
		return histogram0, histogram1, plt_file, delta_h
			
			
			
	def adjustHist(self):
		u"""Сглаживание скачков яркости в проекте"""
		pass
		if not self.hist0:
			self.hist0, self.hist1, plt_file, self.delta_h = self.getHistogram2Graph()
		
		
		to_correct = []
		# пройдёмся по массиву с дельтами и получим разницу между соседними
		for i in xrange(1, len(self.delta_h)):
			delta_delta = abs(self.delta_h[i]- self.delta_h[i-1])
			if delta_delta > 30: #  - поставлено наугад!
				to_correct.append((i, self.delta_h[i])) # создаём список фото, которые надо бы подогнать по яркости под остальные
				
		
		if len(to_correct) > 0:
			for i, dh in to_correct:
				img_to_correct = self.tlImages[i]
				#img_to_correct.adjustHistogram(self.tlImages[i-1])
				img_to_correct.adjustHistogram(self.tlImages[0])
				
		print 'correct ?', to_correct
		
	#http://www.munz.li/2008/07/23/time-lapse-convert-single-jpg-images-to-a-movie/ - надо почитать про режимы сборки менкодером
		
		
class TLImage():
	u""" класс для фото в проекте, должен уметь анализировать себя, давать превьюшку, гистограмму и т.д."""
	def __init__(self, filename="", size=(800,600)):
		self.filename = filename
		print filename
		if filename:
			self.image = io.imread(filename)
		else:
			self.image = numpy.zeros(size)
		
		self.image2 = []
		#self.preview = transform.resize(self.image, (300, 200))
		self.histogram = self.getExp5(), self.getExp4()
		#=self.getHistogram2()
		#self.histogram = []
		
		
		
	def getExp1(self):
		u"""Выдаёт некоторое значение яркости, посчитанное нехитрым образом"""
		a, b, c, d = sum(self.image[1][1]),sum(self.image[-1][1]) ,\
		 sum(self.image[-1][-1]), sum(self.image[1][-1])
		print u'По углам',  a,b,c,d
		
		return sum((a,b,c,d))
	
	def getExp2(self):
		u"""Выдаёт некоторое значение яркости, посчитанное нехитрым образом"""
		w,h = len(self.image[1]), len(self.image)
		
		a, b, c, d = sum(self.image[h/3][w/3]),sum(self.image[2*h/3][w/3]) ,\
		 sum(self.image[2*h/3][2*w/3]), sum(self.image[h/3][2*w/3])
		print u'По 1/3',  a,b,c,d
		
		return sum((a,b,c,d))
	
	def getExp3(self):
		u"""Выдаёт некоторое значение яркости, посчитанное нехитрым образом"""
		w,h = len(self.image[1]), len(self.image)
		
		a, b, c, d = bright(self.image[h/2-h/10][w/2]),bright(self.image[h/2+h/10][w/2]) ,\
		 bright(self.image[h/2-h/10][w/2+w/10]), bright(self.image[h/2+h/10][w/2-w/10])
		print u'По 1/2-1/10',  a,b,c,d
		
		return sum((a,b,c,d))
	
	def getExp4(self):
		u"""Выдаёт некоторое значение яркости, посчитанное нехитрым образом"""
		w,h = len(self.image[1]), len(self.image)
		
		a = sum(self.image[h/2-h/10][w/4:w/3])
		print u'По 1/2 1/4:1/3',  a
		
		return sum(a)
			
			
	def getExp5(self):
		u"""выдаёт сумму яркостей"""
		r = 0
		r1 = 0
		#img = self.image2
		#if len(img) == 0: 
		img = self.image
		if len(self.image2) >0:
			image2 = True
		else:
			image2 = False
			
		print u'сумма яркостей точек через 20\n'
		for x in xrange(0,  len(img)-10, 20):
			print '.',
			for y in xrange(0,  len(img[0]-10), 20):
				r += bright(img[x][y])
				if image2: 
					r1 += bright(self.image2[x][y])
				
		print r, r1,'\t' ,100.0*(r-r1)/r
		return r
		
	def getHistogram2(self):
		u"""Гистограмма по двум столбцам"""
		hv, hc = exposure.histogram(self.preview, nbins=2)
		print 'Histogram for ', self.filename
		print hv, hc
		return hv, hc
	
	
	def getHistogram(self):
		u"""Гистограмма по двум столбцам"""
		hv, hc = exposure.histogram(self.image, nbins=2)
		print 'Histogram for ', self.filename
		print hv, hc
		return hv, hc
	
	
	
	def adjustHistogram(self, img):
		u"""Попробуем изменить яркость.. Надо выдумать ф-ю изменения яркости.
		img - другая картинка, к которой нужно подтянуть яркость.
		Может быть, подойдёт функция skimage.filters.rank.autolevel
		"""	
		print 'adjust histogram for ', self.filename
		
		
		g4 = self.getExp5() * 1.0
		ig4 = img.getExp5() * 1.0
		
		k = g4/ig4
		
		print 'k= ',k
		
		self.image2 = exposure.adjust_gamma(self.image, k, 1)
		
		print abspath(progdir)
		edited_dir = join(abspath(progdir),'edited/' )
		print edited_dir
		if not os.path.exists(edited_dir):
			print 'make dir for edited image', abspath(edited_dir)
			os.makedirs(abspath(edited_dir))
			
			
		io.imsave( join(edited_dir,self.filename.split('/')[-1]), self.image2)
		
		#io.imsave(self.filename, self.image2)
		
		return self#TLImage(self.filename+'_edited.jpg')
		
		
		
	def fromRaw(self, filename):
		# Надо подумать, как сделать загрузку картинки из рава
		# Для начала надо сделать fromTiff
		self.filename = filename

			

	def fromTiff(self, filename):
		self.filename = filename



			
if __name__ == "__main__":
	print 'Test TLImage'
	print u'''Тест функций оценки яркости (по точкам), попытка подогнать яркость img1.jpg под яркость img2.jpg'''
	
	if len(sys.argv) == 3:
		i1 = sys.argv[1]
		i2 = sys.argv[2]
	else:
		i1 = "G0029565.JPG"
		i2 = "G0029564.JPG"
		
	ti1 = TLImage(i1)
	ti2 = TLImage(i2)
	
	#print ti1.getExp1()
	#print ti1.getExp2()
	#print ti1.getExp3()
	e1 = ti1.getExp5()
	e2 = ti2.getExp5()
	print 'e1, e2, %', e1, e2, 100.0*(e1-e2)/e1
	ti1.adjustHistogram(ti2)
	
		
class ShotProject():
	u""" настройки захвата таймлапса """
	def __init__(self, time=1, interval=2):
		self.time = time
		self.interval = interval
